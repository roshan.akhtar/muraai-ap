package com.muraai.apsystem.serviceImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLDataException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.muraai.apsystem.Iservice.InvoiceService;
import com.muraai.apsystem.iDao.InvoiceDAO;
import com.muraai.apsystem.model.InvoiceDetail;
import com.muraai.apsystem.model.InvoiceHeaders;
import com.muraai.apsystem.model.InvoiceSummary;
import com.muraai.apsystem.model.InvoiceThreeWayMatching;
import com.muraai.apsystem.request.InvoiceSummaryRequest;

@Service
public class InvoiceServiceImpl implements InvoiceService {
    
	@Autowired
	private InvoiceDAO invoiceDAOImpl;
	
	@Override
	public List<InvoiceSummary> getInvoiceSummary() throws SQLDataException {
		List<InvoiceSummary> invoiceSummary=null;
    	    
    	   invoiceSummary= invoiceDAOImpl.getInvoiceSummary();
		return invoiceSummary;
	}

	@Override
	public List<InvoiceSummary> searchInvoiceList(InvoiceSummaryRequest request) throws SQLDataException {
		List<InvoiceSummary> invoiceSearchList=null;
		invoiceSearchList =invoiceDAOImpl.searchInvoiceList(request);
		return invoiceSearchList;
	}

	@Override
	public InvoiceHeaders getInvoiceHeaders(InvoiceSummaryRequest request) throws SQLDataException {
		InvoiceHeaders invoiceHeaders = null;
		invoiceHeaders=invoiceDAOImpl.getInvoiceHeaderDetails(request);
		return invoiceHeaders;
	}

	@Override
	public List<InvoiceDetail> getInvoiceDetail(InvoiceSummaryRequest request)throws SQLDataException {
		List<InvoiceDetail> invoiceDetail = null;
		invoiceDetail=invoiceDAOImpl.getInvoiceDetail(request);
		return invoiceDetail;
	}

	@Override
	public List<InvoiceThreeWayMatching> getInvoiceThreeWayMatching(InvoiceSummaryRequest request) throws SQLDataException {
		 List<InvoiceThreeWayMatching> threeWayMatching = null;
		 threeWayMatching=invoiceDAOImpl.getInvoiceThreeWayMatching(request);
		return threeWayMatching;
	}

	@Override
	public void saveUpLoadedInvoiceFile(InvoiceSummaryRequest request)throws IOException {
		StringBuilder builder = new StringBuilder();
        BufferedReader InvoiceReader = null;
        InvoiceReader= new BufferedReader(new FileReader(request.getInvoiceFile()));
        String line = null;
        while((line=InvoiceReader.readLine())!=null)
        {
        	
        }
	}

	
}
