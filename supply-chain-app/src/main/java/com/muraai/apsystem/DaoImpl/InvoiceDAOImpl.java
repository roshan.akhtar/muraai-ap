package com.muraai.apsystem.DaoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.muraai.apsystem.common.constant.QueryConstants;
import com.muraai.apsystem.iDao.InvoiceDAO;
import com.muraai.apsystem.model.InvoiceDetail;
import com.muraai.apsystem.model.InvoiceHeaders;
import com.muraai.apsystem.model.InvoiceSummary;
import com.muraai.apsystem.model.InvoiceThreeWayMatching;
import com.muraai.apsystem.request.InvoiceSummaryRequest;
import com.muraai.apsystem.rowmapper.InvoiceDetailRowmapper;
import com.muraai.apsystem.rowmapper.InvoiceHeadersRowmapper;
import com.muraai.apsystem.rowmapper.InvoiceSummaryRowmapper;
import com.muraai.apsystem.rowmapper.InvoiceThreeWayMatchingRowmapper;
@Repository
public class InvoiceDAOImpl implements InvoiceDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Transactional(readOnly=true)
	@Override
	public List<InvoiceSummary> getInvoiceSummary() {
        return jdbcTemplate.query(
                QueryConstants.INVOICE_SUMMARY,
                 new InvoiceSummaryRowmapper());

	}
	@Override
	@Transactional(readOnly=true)
	public List<InvoiceSummary> searchInvoiceList(InvoiceSummaryRequest request) {
		 Object []searchCriteria=new Object[3];
		 searchCriteria[0]= request.getInvoiceNo();
		 searchCriteria[1]= request.getLinePoNo();
		 searchCriteria[2]= request.getStatus();
		 return jdbcTemplate.query(QueryConstants.INVOICE_SEARCH, searchCriteria,new InvoiceSummaryRowmapper());
	}
	@Transactional(readOnly=true)
	@Override
	public InvoiceHeaders  getInvoiceHeaderDetails(InvoiceSummaryRequest request) {
        Object []invoiceHeader=new Object[2];
        invoiceHeader[0]=request.getInvoiceNo();
        invoiceHeader[1]= request.getInternalId();
		return jdbcTemplate.queryForObject(QueryConstants.INVOICE_HEADER, invoiceHeader,new InvoiceHeadersRowmapper());
	}
	@Transactional(readOnly=true)
	@Override
	public List<InvoiceDetail> getInvoiceDetail(InvoiceSummaryRequest request) {
		 Object []invoiceDetailConditions=new Object[2];
		 invoiceDetailConditions[0]=request.getInvoiceNo();
		 invoiceDetailConditions[1]=request.getInternalId();
		return jdbcTemplate.query(QueryConstants.INVOICE_DETAIL,invoiceDetailConditions,new InvoiceDetailRowmapper());
	}
	@Transactional(readOnly=true)
	@Override
	public List<InvoiceThreeWayMatching> getInvoiceThreeWayMatching(InvoiceSummaryRequest request) {
		 Object []threeWayMatchingConditions=new Object[2];
		 threeWayMatchingConditions[0]=request.getInvoiceNo();
		 threeWayMatchingConditions[1]=request.getLinePoNo();
		 return jdbcTemplate.query(QueryConstants.INVOICE_THREE_WAY_MATCHING,threeWayMatchingConditions,new InvoiceThreeWayMatchingRowmapper());
	}

	
  
}
