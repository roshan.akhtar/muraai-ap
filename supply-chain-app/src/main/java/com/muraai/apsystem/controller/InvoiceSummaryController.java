package com.muraai.apsystem.controller;
import java.sql.SQLDataException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.muraai.apsystem.Iservice.InvoiceService;
import com.muraai.apsystem.model.InvoiceDetail;
import com.muraai.apsystem.model.InvoiceHeaders;
import com.muraai.apsystem.model.InvoiceSummary;
import com.muraai.apsystem.model.InvoiceThreeWayMatching;
import com.muraai.apsystem.request.InvoiceSummaryRequest;

@Controller
public class InvoiceSummaryController {

	public final static Logger LOG = Logger.getLogger(InvoiceSummaryController.class);
	@Autowired
	private InvoiceService invoiceServices;
	
	@RequestMapping(value = "/services/invoice-summary", method = RequestMethod.GET, produces="application/json")
    @ResponseBody   
	public List<InvoiceSummary> getInvoiceSummary() throws SQLDataException {
                
		List<InvoiceSummary> list = invoiceServices.getInvoiceSummary();
		LOG.debug("In InvoiceSummary Controller");
		return list;
    }
	@RequestMapping(value = "/services/invoice-search", method = RequestMethod.POST,consumes="application/json", produces="application/json")
    @ResponseBody  
    public List<InvoiceSummary> searchInvoiceSummary(@RequestBody InvoiceSummaryRequest request) throws SQLDataException {
		
		List<InvoiceSummary> searchList=invoiceServices.searchInvoiceList(request);
		return searchList;
	} 
    
	@RequestMapping(value = "/services/invoice-header", method = RequestMethod.POST,consumes="application/json", produces="application/json")
    @ResponseBody   
	public InvoiceHeaders getInvoiceHeader(@RequestBody InvoiceSummaryRequest request) throws SQLDataException{
		InvoiceHeaders headers = invoiceServices.getInvoiceHeaders(request);
		return headers;
	}
	
	@RequestMapping(value = "/services/invoice-detail", method = RequestMethod.POST,consumes="application/json", produces="application/json")
    @ResponseBody   
	public List<InvoiceDetail> getInvoiceDetail(@RequestBody InvoiceSummaryRequest request) throws SQLDataException{
		List<InvoiceDetail> invoiceDetail = invoiceServices.getInvoiceDetail(request);
		return invoiceDetail;
	}
	
	@RequestMapping(value = "/services/invoice-threeWayMatching", method = RequestMethod.POST,consumes="application/json", produces="application/json")
    @ResponseBody
    public List<InvoiceThreeWayMatching> getInvoiceThreeWayMatching(@RequestBody InvoiceSummaryRequest request)throws SQLDataException{
		List<InvoiceThreeWayMatching> invoiceThreeWayMatching = invoiceServices.getInvoiceThreeWayMatching(request);
		return invoiceThreeWayMatching;
	}
	@RequestMapping(value = "/services/invoice-upload", method = RequestMethod.POST,consumes="application/image", produces="application/json")
    @ResponseBody
    public List<InvoiceThreeWayMatching> saveUpLoadedInvoiceFile(@RequestBody InvoiceSummaryRequest request)throws SQLDataException{
		List<InvoiceThreeWayMatching> invoiceThreeWayMatching = invoiceServices.getInvoiceThreeWayMatching(request);
		return invoiceThreeWayMatching;
	}
}
