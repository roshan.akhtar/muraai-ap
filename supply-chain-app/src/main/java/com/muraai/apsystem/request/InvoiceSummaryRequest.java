package com.muraai.apsystem.request;

import java.io.File;

public class InvoiceSummaryRequest implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1320895364973198707L;
	private String invoiceNo;
	private String internalId;
	private String linePoNo;
	private String status;
	private File invoiceFile;
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getInternalId() {
		return internalId;
	}
	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getLinePoNo() {
		return linePoNo;
	}
	public void setLinePoNo(String linePoNo) {
		this.linePoNo = linePoNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public File getInvoiceFile() {
		return invoiceFile;
	}
	public void setInvoiceFile(File invoiceFile) {
		this.invoiceFile = invoiceFile;
	}
	
}
