package com.muraai.apsystem.iDao;

import java.util.List;

import com.muraai.apsystem.model.InvoiceDetail;
import com.muraai.apsystem.model.InvoiceHeaders;
import com.muraai.apsystem.model.InvoiceSummary;
import com.muraai.apsystem.model.InvoiceThreeWayMatching;
import com.muraai.apsystem.request.InvoiceSummaryRequest;


public interface InvoiceDAO {

	List <InvoiceSummary> getInvoiceSummary();
	List <InvoiceSummary> searchInvoiceList(InvoiceSummaryRequest request);
	InvoiceHeaders getInvoiceHeaderDetails(InvoiceSummaryRequest request);
	List<InvoiceDetail> getInvoiceDetail(InvoiceSummaryRequest request);
	List<InvoiceThreeWayMatching> getInvoiceThreeWayMatching(InvoiceSummaryRequest request);
}
