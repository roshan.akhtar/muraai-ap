package com.muraai.apsystem.model;

import java.io.Serializable;

public class InvoiceThreeWayMatching implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4128293569510283886L;
	private String poLine;
	private String invoiceLine;
	private String material;
	private String materialDesc;
	private String poLineQuantity;
	private String invoiceQuantity;
	private String goodRecievedQuntity;
	private String goodRejectedQuantity;
	private String balancePoQuantity;
	private double poLineRate;
	private double invoiceLineRate;
	private double goodRecievedAmount;
	private double toBePaid;
	private String reason;
	private double invoiceAmount;
	private String threeWayMatching;
	public String getPoLine() {
		return poLine;
	}
	public void setPoLine(String poLine) {
		this.poLine = poLine;
	}
	public String getInvoiceLine() {
		return invoiceLine;
	}
	public void setInvoiceLine(String invoiceLine) {
		this.invoiceLine = invoiceLine;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getMaterialDesc() {
		return materialDesc;
	}
	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
	public String getPoLineQuantity() {
		return poLineQuantity;
	}
	public void setPoLineQuantity(String poLineQuantity) {
		this.poLineQuantity = poLineQuantity;
	}
	public String getInvoiceQuantity() {
		return invoiceQuantity;
	}
	public void setInvoiceQuantity(String invoiceQuantity) {
		this.invoiceQuantity = invoiceQuantity;
	}
	 
	public String getBalancePoQuantity() {
		return balancePoQuantity;
	}
	public void setBalancePoQuantity(String balancePoQuantity) {
		this.balancePoQuantity = balancePoQuantity;
	}
	public double getPoLineRate() {
		return poLineRate;
	}
	public void setPoLineRate(double poLineRate) {
		this.poLineRate = poLineRate;
	}
	public double getInvoiceLineRate() {
		return invoiceLineRate;
	}
	public void setInvoiceLineRate(double invoiceLineRate) {
		this.invoiceLineRate = invoiceLineRate;
	}
	public double getToBePaid() {
		return toBePaid;
	}
	public void setToBePaid(double toBePaid) {
		this.toBePaid = toBePaid;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getThreeWayMatching() {
		return threeWayMatching;
	}
	public void setThreeWayMatching(String threeWayMatching) {
		this.threeWayMatching = threeWayMatching;
	}
	public String getGoodRecievedQuntity() {
		return goodRecievedQuntity;
	}
	public void setGoodRecievedQuntity(String goodRecievedQuntity) {
		this.goodRecievedQuntity = goodRecievedQuntity;
	}
	public String getGoodRejectedQuantity() {
		return goodRejectedQuantity;
	}
	public void setGoodRejectedQuantity(String goodRejectedQuantity) {
		this.goodRejectedQuantity = goodRejectedQuantity;
	}
	public double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public double getGoodRecievedAmount() {
		return goodRecievedAmount;
	}
	public void setGoodRecievedAmount(double goodRecievedAmount) {
		this.goodRecievedAmount = goodRecievedAmount;
	}
	
	
}