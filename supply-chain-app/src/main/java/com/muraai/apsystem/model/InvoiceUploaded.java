package com.muraai.apsystem.model;

import java.io.Serializable;

public class InvoiceUploaded implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8483725306780756271L;
	private String fileName;
	private String invoiceNo;
    private String fileType;
    private String internalDocId;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getInternalDocId() {
		return internalDocId;
	}
	public void setInternalDocId(String internalDocId) {
		this.internalDocId = internalDocId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
}
