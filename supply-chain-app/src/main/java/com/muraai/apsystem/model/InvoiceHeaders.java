package com.muraai.apsystem.model;

import java.sql.Date;

public class InvoiceHeaders implements java.io.Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private String invoiceNo;
    private String poNumber;
    private long poValue;
    private long totalInvoiceGrossValue;
    private Date invoiceDate;
    private String invoiceStatus;
    private long netInvoiceValue;
    private long tax;
    private String currency;
    private String vendor;
    private long grossInvoiceValue;
    private long remainingPoValue;
    private String threeWayMatchingStatus;
    private long tillDateClearedAmountDiscount;
    private String paymentTerms;
    private long amountTobePaid;
    private String line;
    private String material;
    private String materialDesc;
    private String quantity;
    private String rate;
    private long taxPercentage;
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public long getPoValue() {
		return poValue;
	}
	public void setPoValue(long poValue) {
		this.poValue = poValue;
	}
	public long getTotalInvoiceGrossValue() {
		return totalInvoiceGrossValue;
	}
	public void setTotalInvoiceGrossValue(long totalInvoiceGrossValue) {
		this.totalInvoiceGrossValue = totalInvoiceGrossValue;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public long getNetInvoiceValue() {
		return netInvoiceValue;
	}
	public void setNetInvoiceValue(long netInvoiceValue) {
		this.netInvoiceValue = netInvoiceValue;
	}
	public long getTax() {
		return tax;
	}
	public void setTax(long tax) {
		this.tax = tax;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public long getGrossInvoiceValue() {
		return grossInvoiceValue;
	}
	public void setGrossInvoiceValue(long grossInvoiceValue) {
		this.grossInvoiceValue = grossInvoiceValue;
	}
	public long getRemainingPoValue() {
		return remainingPoValue;
	}
	public void setRemainingPoValue(long remainingPoValue) {
		this.remainingPoValue = remainingPoValue;
	}
	public String getThreeWayMatchingStatus() {
		return threeWayMatchingStatus;
	}
	public void setThreeWayMatchingStatus(String threeWayMatchingStatus) {
		this.threeWayMatchingStatus = threeWayMatchingStatus;
	}
	public long getTillDateClearedAmountDiscount() {
		return tillDateClearedAmountDiscount;
	}
	public void setTillDateClearedAmountDiscount(
			long tillDateClearedAmountDiscount) {
		this.tillDateClearedAmountDiscount = tillDateClearedAmountDiscount;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public long getAmountTobePaid() {
		return amountTobePaid;
	}
	public void setAmountTobePaid(long amountTobePaid) {
		this.amountTobePaid = amountTobePaid;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getMaterialDesc() {
		return materialDesc;
	}
	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public long getTaxPercentage() {
		return taxPercentage;
	}
	public void setTaxPercentage(long taxPercentage) {
		this.taxPercentage = taxPercentage;
	}
    
    
     
}
