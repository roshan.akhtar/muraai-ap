package com.muraai.apsystem.model;

import java.io.Serializable;

public class InvoiceDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2902043648908880125L;
	
	private String line;
	private String material;
	private String materialDesc;
	private String  quantity;
	private double rate;
	private double taxPercentage;
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getMaterialDesc() {
		return materialDesc;
	}
	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getTaxPercentage() {
		return taxPercentage;
	}
	public void setTaxPercentage(double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
