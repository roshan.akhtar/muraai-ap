package com.muraai.apsystem.Iservice;

import java.io.IOException;
import java.sql.SQLDataException;
import java.util.List;

import com.muraai.apsystem.model.InvoiceDetail;
import com.muraai.apsystem.model.InvoiceHeaders;
import com.muraai.apsystem.model.InvoiceSummary;
import com.muraai.apsystem.model.InvoiceThreeWayMatching;
import com.muraai.apsystem.request.InvoiceSummaryRequest;

public interface InvoiceService {

	List <InvoiceSummary> getInvoiceSummary() throws SQLDataException;
	List <InvoiceSummary> searchInvoiceList(InvoiceSummaryRequest request) throws SQLDataException;
	InvoiceHeaders getInvoiceHeaders(InvoiceSummaryRequest request) throws SQLDataException;
	List<InvoiceDetail> getInvoiceDetail(InvoiceSummaryRequest request) throws SQLDataException;
	List<InvoiceThreeWayMatching> getInvoiceThreeWayMatching(InvoiceSummaryRequest request) throws SQLDataException;
	void saveUpLoadedInvoiceFile(InvoiceSummaryRequest request) throws IOException;
}
