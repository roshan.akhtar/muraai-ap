package com.muraai.apsystem.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.muraai.apsystem.model.InvoiceThreeWayMatching;

public class InvoiceThreeWayMatchingRowmapper implements RowMapper<InvoiceThreeWayMatching> {

	@Override
	public InvoiceThreeWayMatching mapRow(ResultSet resultset, int rowNum) throws SQLException {
		InvoiceThreeWayMatching invoiceThreeWayMatching = new InvoiceThreeWayMatching();
		invoiceThreeWayMatching.setPoLine(resultset.getString("POLine"));
		invoiceThreeWayMatching.setInvoiceLine(resultset.getString("Inv_Line"));
		invoiceThreeWayMatching.setMaterial(resultset.getString("Material"));
		invoiceThreeWayMatching.setMaterialDesc(resultset.getString("line_material_desc"));
		invoiceThreeWayMatching.setPoLineQuantity(resultset.getString("line_order_qty"));
		invoiceThreeWayMatching.setInvoiceQuantity(resultset.getString("InvoiceQty"));
		invoiceThreeWayMatching.setGoodRecievedQuntity(resultset.getString("GRNQty"));
		invoiceThreeWayMatching.setGoodRejectedQuantity(resultset.getString("GRNRejQty"));
		invoiceThreeWayMatching.setBalancePoQuantity(resultset.getString("POBalanceQty"));
		invoiceThreeWayMatching.setPoLineRate(resultset.getDouble("PO_Line_Rate"));
		invoiceThreeWayMatching.setInvoiceLineRate(resultset.getDouble("Invoice_Line_Rate"));
		invoiceThreeWayMatching.setInvoiceAmount(resultset.getDouble("InvoiceAmount"));
		invoiceThreeWayMatching.setGoodRecievedAmount(resultset.getDouble("GRNAmount"));
		invoiceThreeWayMatching.setToBePaid(resultset.getDouble("GRNRejAmount"));
		invoiceThreeWayMatching.setReason(resultset.getString("Reason"));
		invoiceThreeWayMatching.setThreeWayMatching(resultset.getString("ThreeWayMatchingStatus"));
		return invoiceThreeWayMatching;
	}

}
