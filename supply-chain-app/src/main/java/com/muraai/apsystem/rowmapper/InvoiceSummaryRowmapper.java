package com.muraai.apsystem.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.muraai.apsystem.model.InvoiceSummary;

public class InvoiceSummaryRowmapper implements  RowMapper<InvoiceSummary>{

	@Override
	public InvoiceSummary mapRow(ResultSet resultset, int rowNum) throws SQLException {
		
		InvoiceSummary invoiceSummary = new InvoiceSummary();
		invoiceSummary.setInvoiceNo(resultset.getString("invoice_no"));
		invoiceSummary.setInvoiceType(resultset.getString("Inv_Type"));
		invoiceSummary.setInvoiceDate(resultset.getDate("invoice_date"));
		invoiceSummary.setVendorName(resultset.getString("win_vendor_master.vendor_name"));
		invoiceSummary.setVendorCode(resultset.getString("vendor"));
		invoiceSummary.setPoNumber(resultset.getString("invoice_po"));
		invoiceSummary.setInvoiceValue(resultset.getLong("invoice_value"));
		invoiceSummary.setTax(resultset.getLong("tax_per"));
		invoiceSummary.setPendingWith(resultset.getString("pending_with"));
		invoiceSummary.setInvoiceStatus(resultset.getString("invoice_status"));
		invoiceSummary.setErrorDescription(resultset.getString("err_desc"));
		return invoiceSummary;
		 	
	}

}
