package com.muraai.apsystem.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.muraai.apsystem.model.InvoiceHeaders;

public class InvoiceHeadersRowmapper implements RowMapper<InvoiceHeaders> {

	@Override
	public InvoiceHeaders mapRow(ResultSet resultset, int rowNum) throws SQLException {
		InvoiceHeaders invoiceHeaders = new InvoiceHeaders();
		invoiceHeaders.setInvoiceNo(resultset.getString("invoice_no"));
		invoiceHeaders.setPoNumber(resultset.getString("invoice_po"));
		invoiceHeaders.setPoValue(resultset.getLong("POValue"));
		// there is some doubt in table collumns
		invoiceHeaders.setTotalInvoiceGrossValue(resultset.getLong("POValue"));
		invoiceHeaders.setNetInvoiceValue(resultset.getLong("POValue"));
		invoiceHeaders.setGrossInvoiceValue(resultset.getLong("POValue"));
		invoiceHeaders.setRemainingPoValue(resultset.getLong("POValue"));
		invoiceHeaders.setAmountTobePaid(resultset.getLong("totalDiscount"));
		
		invoiceHeaders.setInvoiceDate(resultset.getDate("invoice_date"));
		invoiceHeaders.setInvoiceStatus(resultset.getString("invoice_status"));
		//invoiceHeaders.setTax(resultset.getLong("win_tax_desc"));
		invoiceHeaders.setCurrency(resultset.getString("currency"));
		invoiceHeaders.setVendor(resultset.getString("VendorName"));
		invoiceHeaders.setThreeWayMatchingStatus(resultset.getString("three_way_matching_status"));
		invoiceHeaders.setTillDateClearedAmountDiscount(resultset.getLong("totalDiscount"));
		invoiceHeaders.setPaymentTerms(resultset.getString("payment_terms"));
		
		return invoiceHeaders;
	}

}
