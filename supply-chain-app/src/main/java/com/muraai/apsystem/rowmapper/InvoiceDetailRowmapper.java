package com.muraai.apsystem.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.muraai.apsystem.model.InvoiceDetail;

public class InvoiceDetailRowmapper implements RowMapper<InvoiceDetail>{

	@Override
	public InvoiceDetail mapRow(ResultSet resultset, int rowNume) throws SQLException {
		InvoiceDetail invoiceDetail = new InvoiceDetail();
		invoiceDetail.setLine(resultset.getString("line_no"));
		invoiceDetail.setMaterial(resultset.getString("material"));
		invoiceDetail.setMaterialDesc(resultset.getString("material_desc"));
		invoiceDetail.setQuantity(resultset.getString("qty"));
		invoiceDetail.setRate(resultset.getDouble("rate"));
		invoiceDetail.setTaxPercentage(resultset.getDouble("tax"));
		return invoiceDetail;
	}

}
