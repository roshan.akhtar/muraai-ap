package com.muraai.apsystem.response;

import com.muraai.apsystem.model.InvoiceSummary;

public class InvoiceResponse {
	
	private InvoiceSummary invoiceSummary;

	public InvoiceSummary getInvoiceSummary() {
		return invoiceSummary;
	}

	public void setInvoiceSummary(InvoiceSummary invoiceSummary) {
		this.invoiceSummary = invoiceSummary;
	}
	

}
