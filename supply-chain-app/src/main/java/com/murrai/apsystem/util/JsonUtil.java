package com.murrai.apsystem.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muraai.apsystem.model.InvoiceSummary;
import com.muraai.apsystem.response.InvoiceResponse;

public class JsonUtil {

	/*public static void main(String[] args) {
		//JsonUtil jsonUtil = new JsonUtil();
		//jsonUtil.run();
		double m=123.90;
		System.out.print(m);
	}*/
	private void run() {
		ObjectMapper mapper = new ObjectMapper();

		InvoiceSummary invoiceSummary = createDummyInvoiceSummaryObject();
		InvoiceResponse invoiceResponse = new InvoiceResponse();
		invoiceResponse.setInvoiceSummary(invoiceSummary);
		try {

			// Convert object to JSON string
			String jsonInString = mapper.writeValueAsString(invoiceResponse);
			System.out.println(jsonInString);

			// Convert object to JSON string and pretty print
			jsonInString = mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(invoiceResponse);
			System.out.println(jsonInString);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private InvoiceSummary createDummyInvoiceSummaryObject() {

		InvoiceSummary invoiceSummary = new InvoiceSummary();
        invoiceSummary.setErrorDescription("sample error");
        invoiceSummary.setInvoiceNo("100");
		return invoiceSummary;

	}

}
