package com.muraai.apsystem.test.dao;


import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.muraai.apsystem.config.AccountPayableApplication;
import com.muraai.apsystem.iDao.InvoiceDAO;
import com.muraai.apsystem.model.InvoiceSummary;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(AccountPayableApplication.class)
public class TestInvoiceDAO {

	@Autowired
	protected InvoiceDAO invoiceimpl;
	
	
	@Test
	public void findAllInvoiceDetails()
	{
		List<InvoiceSummary> invoiceList = invoiceimpl.getInvoiceSummary();
		Assert.notNull(invoiceList.isEmpty());
	}
	
}
